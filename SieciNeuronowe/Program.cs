﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieciNeuronowe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---------------------");
            Obliczanie o1 = new Obliczanie(1, 1, 0);
            Obliczanie o2 = new Obliczanie(1, 0, 0);
            Obliczanie o3 = new Obliczanie(0, 1, 0);
            Obliczanie o4 = new Obliczanie(1, 1, 0);

            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Obliczanie o11 = new Obliczanie(1, 1, 1);
            Obliczanie o22 = new Obliczanie(1, 0, 1);
            Obliczanie o33 = new Obliczanie(0, 1, 1);
            Obliczanie o44 = new Obliczanie(1, 1, 1);

            Console.ReadKey();
        }
    }
}
