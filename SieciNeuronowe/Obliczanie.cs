﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieciNeuronowe
{
    class Obliczanie
    {
        Wagi waga;
        Wektor wektor;
        int x1, x2, Wynik;
        double w1, w2, b;

        public Obliczanie(int a, int b, int c)
        {
            wektor = new Wektor(a, b, c);
            waga = new Wagi();
            x1 = wektor.ReturnX();
            x2 = wektor.ReturnY();
            Wynik = wektor.ReturnZ();
            w1 = waga.ReturnW1();
            w2 = waga.ReturnW2();
            b = waga.ReturnB();
            licz();

        }
        public void licz()
        {
            double Suma = 0;
            Suma = suma();
            int licznik = 0;
            while(Suma != Wynik)
            {
                Modyfikacja(Suma);
                Suma = suma();
                licznik++;
                Console.WriteLine("nr." + licznik+", suma: " + w1+ + w2+ b);
                //Console.WriteLine("nr." + licznik+", w1: " + w1+ ", w2: " + w2+ ", b: " + b);
            }

            Console.WriteLine("Udało się po " + licznik + " próbach.");
            Console.WriteLine("--------------------------------------------------------");

        }
        public double suma()
        {
            if ((x1 * w1 + x2 * w2 + b) > 0)
                return 1;
            else
                return 0;
        }
        public void Modyfikacja(double Suma)
        {
            w1 += 0.1 * (Wynik - Suma) * x1;
            w2 += 0.1 * (Wynik - Suma) * x2;
            b  += 0.1 * (Wynik - Suma);
        }

    }
}
