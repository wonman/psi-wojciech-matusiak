﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieciNeuronowe
{
    class Wagi
    {
        int w1, w2, b;
        Random random= new Random();
        public Wagi()
        {
            w1 = random.Next(0, 10);
            w2 = random.Next(0, 10);
            b = random.Next(0, 10);
        }
        public void losuj()
        {
            w1 = random.Next(0, 10);
            w2 = random.Next(0, 10);
            b = random.Next(0, 1);
        }
        public int ReturnW1()
        {
            return w1;
        }
        public int ReturnW2()
        {
            return w2;
        }
        public int ReturnB()
        {
            return b;
        }

    }
}
