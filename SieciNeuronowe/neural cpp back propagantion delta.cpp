// neural cpp back propagantion deltka.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib> 
#include <iostream>
#include <ctime>

using namespace std;


int main()
{
	cout << "hello"<<endl;
	system("pause");
    return 0;
}
class CBackProp {

	
	double **out;
	double **deltka;
	double ***weight;
	int numl;
	int *lsize;
	double beta;
	double alpha;
	double ***prevDwt;
	double sigmoid(double in);

public:

	~CBackProp();
	CBackProp(int nl, int *sz, double b, double a);
	void bpgt(double *in, double *tgt);
	void ffwd(double *in);
	double mse(double *tgt);
	double Out(int i) const;
};
CBackProp::CBackProp(int nl, int *sz, double b, double a) :beta(b), alpha(a)
{

	numl = nl;
	lsize = new int[numl];

	for (int i = 0; i<numl; i++) {
		lsize[i] = sz[i];
	}
	out = new double*[numl];

	for (int i = 0; i<numl; i++) {
		out[i] = new double[lsize[i]];
	}
	deltka = new double*[numl];

	for (int i = 1; i<numl; i++) {
		deltka[i] = new double[lsize[i]];
	}

	weight = new double**[numl];

	for (int i = 1; i<numl; i++) {
		weight[i] = new double*[lsize[i]];
	}
	for (int i = 1; i<numl; i++) {
		for (int j = 0; j<lsize[i]; j++) {
			weight[i][j] = new double[lsize[i - 1] + 1];
		}
	}
	prevDwt = new double**[numl];
	int i = 0;
	for (i = 1; i<numl; i++) {
		prevDwt[i] = new double*[lsize[i]];

	}
	for (i = 1; i<numl; i++) {
		for (int j = 0; j<lsize[i]; j++) {
			prevDwt[i][j] = new double[lsize[i - 1] + 1];
		}
	}

	srand((unsigned)(time(NULL)));
	for (i = 1; i<numl; i++)
		for (int j = 0; j<lsize[i]; j++)
			for (int k = 0; k<lsize[i - 1] + 1; k++)
				weight[i][j][k] = (double)(rand()) / (RAND_MAX / 2) - 1;

	for (i = 1; i<numl; i++)
		for (int j = 0; j<lsize[i]; j++)
			for (int k = 0; k<lsize[i - 1] + 1; k++)
				prevDwt[i][j][k] = (double)0.0;
}
void CBackProp::ffwd(double *in)
{
	double sum;

	

	for (int i = 0; i < lsize[0]; i++)
		out[0][i] = in[i];


	for (int i = 1; i < numl; i++) {  
		for (int j = 0; j < lsize[i]; j++) {
			sum = 0.0;
	
			for (int k = 0; k < lsize[i - 1]; k++) {
			
				sum += out[i - 1][k] * weight[i][j][k];
			}
			
			sum += weight[i][j][lsize[i - 1]];
			
			out[i][j] = sigmoid(sum);
		}
	}
}
void CBackProp::bpgt(double *in, double *tgt)
{
	double sum;
	ffwd(in);
	for (int i = 0; i < lsize[numl - 1]; i++) {
		deltka[numl - 1][i] = out[numl - 1][i] *
			(1 - out[numl - 1][i])*(tgt[i] - out[numl - 1][i]);
	}
	int i = 0;
	for (i = numl - 2; i > 0; i--) {
		for (int j = 0; j < lsize[i]; j++) {
			sum = 0.0;
			for (int k = 0; k < lsize[i + 1]; k++) {
				sum += deltka[i + 1][k] * weight[i + 1][k][j];
			}
			deltka[i][j] = out[i][j] * (1 - out[i][j])*sum;
		}
	}
	for (i = 1; i < numl; i++) {
		for (int j = 0; j < lsize[i]; j++) {
			for (int k = 0; k < lsize[i - 1]; k++) {
				weight[i][j][k] += alpha*prevDwt[i][j][k];
			}
			weight[i][j][lsize[i - 1]] += alpha*prevDwt[i][j][lsize[i - 1]];
		}
	}
	for (i = 1; i < numl; i++) {
		for (int j = 0; j < lsize[i]; j++) {
			for (int k = 0; k < lsize[i - 1]; k++) {
				prevDwt[i][j][k] = beta*deltka[i][j] * out[i - 1][k];
				weight[i][j][k] += prevDwt[i][j][k];
			}
			prevDwt[i][j][lsize[i - 1]] = beta*deltka[i][j];
			weight[i][j][lsize[i - 1]] += prevDwt[i][j][lsize[i - 1]];
		}
	}
}
