﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieciNeuronowe
{
    class Wektor
    {
        int x, y, z;
        public Wektor(int a, int b, int c)
        {
            this.x = a;
            this.y = b;
            this.z = c;
        }
        public int ReturnX()
        {
            return x;
        }
        public int ReturnY()
        {
            return y;
        }
        public int ReturnZ()
        {
            return z;
        }


    }
}
